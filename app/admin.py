# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django.contrib import admin
from django.contrib.admin.decorators import register
from .forms import *

# Register your models here.
admin.site.register(empleado)
