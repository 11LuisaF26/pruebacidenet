from django.forms import ModelForm
from django.db import models
from django import forms
from .models import *

class empleado_form(ModelForm):
    class Meta():
        model = empleado
        fields = (
            'primer_apellido', 
            'segundo_apellido', 
            'primer_nombre',
            'otros_nombres', 
            'pais_empleo', 
            'tipo_identificacion', 
            'numero_identificacion', 
            'correo_electronico',
            'fecha_ingreso',
            'area',
            'estado',
            'fecha_registro')