# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django.urls import path, re_path
from app import views

urlpatterns = [

    # The home page
    path('', views.home, name='home'),

    # Matches any html file
    #re_path(r'^.*\.*', views.pages, name='pages'),
    path('crear/', views.add_empleado,  name='crear_empleado'),
    path('consultar/', views.listar_empleados,  name='consultar_empleado'),
    re_path(r'^consultar/delete/(?P<id>\w+)/$', views.delete_empleado, name='eliminar_empleado'),
    re_path(r'^empresas/save/(?P<id>\w+)/$', views.add_empleado, name='editar_empleado'),
]
