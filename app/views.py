# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from .forms import *
from .models import *
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from django.template import loader
from django.http import HttpResponse, request
from django import forms, template
from django.db.models import Q

def home(request):
    return render(request, "home.html")

def add_empleado(request, id=0):
    msg     = None
    success = False     
    form = empleado_form()    
           
    if request.method == 'GET':
        if id==0:
            form = empleado_form()
        else:
            emp = empleado.objects.get(pk=id)
            form = empleado_form(instance = emp)                
        return render(request, "crear_empleado.html", {"form": form, "msg" : msg, "success" : success }) 
    else:
        if id==0:
            form = empleado_form(request.POST)
        else:                 
            emp = empleado.objects.get(pk=id)
            form = empleado_form(request.POST, instance = emp)

        if form.is_valid():
            form.save()
            msg     = 'Empresa guardada.'
            success = True
            return render(request, "crear_empleado.html", {"form": form, "msg" : msg, "success" : success }) 
        return render(request, "crear_empleado.html", {"form": form, "msg" : msg, "success" : success })  
    

def listar_empleados(request):
    busqueda = request.POST.get('buscar','')
    empleados = empleado.objects.all()
    if busqueda:
        empleados = empleado.objects.filter(
            Q(primer_apellido__icontains = busqueda) |
            Q(segundo_apellido__icontains = busqueda) |
            Q(primer_nombre__icontains = busqueda) |
            Q(otros_nombres__icontains = busqueda) |
            Q(tipo_identificacion__icontains = busqueda) |
            Q(numero_identificacion__icontains = busqueda) |
            Q(pais_empleo__icontains = busqueda) |
            Q(correo_electronico__icontains = busqueda) |
            Q(estado__icontains = busqueda)        
        )
    return render(request, "empleados.html", {"empleados": empleados})  

def delete_empleado(request, id=0):
    msg     = None
    success = False      
    form = empleado_form() 
    emp = empleado.objects.get(pk=id)
    emp.delete()
    msg     = 'Empresa eliminada.'
    success = True
    return render(request, 'empleados.html', {'form': form, "msg" : msg, "success" : success })