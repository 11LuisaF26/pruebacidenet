# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django.db import models
from django.core.validators import RegexValidator

alfabetico = RegexValidator(r'^[A-Z]*$','Only alphabetic characters are allowed.')
numerico = RegexValidator(r'^[0-9]*$','Only numeric characters are allowed.')
alfanumerico = RegexValidator(r'^[0-9a-zA-Z-]*$', 'Only alphanumeric characters are allowed.')
alfanumerico2 = RegexValidator(r'^[0-9a-zA-Z.-@]*$', 'Only alphanumeric characters are allowed.')

# Create your models here.
class empleado(models.Model):
    primer_apellido = models.CharField(max_length=20, validators=[alfabetico],  blank=False, null=False, verbose_name='Primer Apellido')
    segundo_apellido = models.CharField(max_length=20, validators=[alfabetico],  blank=False, null=False, verbose_name='Segundo Apellido')
    primer_nombre = models.CharField(max_length=20,validators=[alfabetico],  blank=False, null=False, verbose_name='Primer Nombre')
    otros_nombres = models.CharField(max_length=50, validators=[alfabetico], null=True, blank=True, verbose_name='Otros Nombres')
    pais_empleo = models.CharField(max_length=15, null=False, verbose_name='Pais Empleo')
    tipo_identificacion = models.CharField(max_length=30, blank=False, null=False,verbose_name='Tipo de identifiacion')
    numero_identificacion = models.IntegerField(unique=True, blank=False,null=False, validators=[alfanumerico], verbose_name='Numero de identifiacion')
    correo_electronico = models.CharField(max_length=300, blank=False, null=False, validators=[alfanumerico2], verbose_name='Correo Electronico')
    fecha_ingreso = models.DateField(max_length=10, blank=False,  null=False, verbose_name='Fecha de Ingreso')
    area = models.CharField(max_length=300, blank=False,  null=False, validators=[alfanumerico], verbose_name='Area')
    estado = models.CharField(max_length=10, blank=False, null=False, validators=[alfanumerico2], verbose_name='Estado')
    fecha_registro = models.DateField(max_length=18, blank=False, null= False, verbose_name='Fecha y hora de registro')
    

    class Meta():
        verbose_name = "empleado"
        verbose_name_plural = "empleado"

    def __str__(self):
        return self.primer_apellido
